<?php

$config = new OC\CodingStandard\Config();

$config
    ->setUsingCache(true)
    ->getFinder()
    ->in(__DIR__)
    ->exclude('node_modules')
    ->exclude('vendor-libs')
    ->exclude('build')
    ;

return $config;
