
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const path = require('path');

module.exports = {

	mode: 'development',

	entry: './src/index.js',

	output: {
		filename: 'main.js',
		path: path.resolve(__dirname, 'js'),
	},

	module: {
		rules: [
			{
				test: /\.css$/,
				use: [
				'style-loader',
				'css-loader',
				],
			},
			{
				test: /\.vue$/,
				loader: 'vue-loader',
			},
		],
	},

	plugins: [
		new VueLoaderPlugin()
	]
};
