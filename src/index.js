
'use strict';

import Vue from 'vue';
import App from './App.vue';

export const eventBus = new Vue();

window.onload = () => new Vue({
  render: h => h(App),
}).$mount('#app-content');
