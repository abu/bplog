
'use strict';

var isValidDate = function (ts) {
  return ts instanceof Date && !isNaN(ts);
}

export function validateTimestamp(ts) {
  if (isValidDate(ts)) {
    return true;
  } else {
    OC.Notification.showTemporary(t('bplog', 'Invalid date'));
    return false;
  }
}

export function getLocalTimeStamp(ts) {
  ts.setMinutes(ts.getMinutes() - ts.getTimezoneOffset());
  var str = ts.toISOString();
  return str.slice(0, 10) + ' ' + str.slice(11, 19);
}

export function sortLog(data, newest) {
  data.sort(newest ?
    function (a, b) {
      return b.created - a.created;
    } :
    function (a, b) {
      return a.created - b.created;
    },
  );
}
