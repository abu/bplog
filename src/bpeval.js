'use strict';

import { Evaluator } from './evaluator.js'

export default
class BloodPressureEvaluator extends Evaluator {
  constructor(mode = 0) {
    super(mode ? threshWHO : threshSwissHearts)
  }
  evaluate(sys, dia) {
    var value = Math.max(
      this.index(sys, this._thresholds[0]),
      this.index(dia, this._thresholds[1]),
    );

    // For 3-stage scales: Skip the value 2 (orange), return 3 (red) instead.
    return this._thresholds[0].length == 2 && value == 2 ? 3 : value;
  }
}

const threshWHO = [
  [130, 140],
  [85, 90],
];

const threshSwissHearts = [
  [140, 160, 180],
  [90, 100, 110],
];
