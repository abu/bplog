'use strict';

export class Evaluator {
  constructor(thresholds) {
    this._thresholds = thresholds;
  }

  index(value, thresholds) {
    var i;
    for (i = thresholds.length - 1; i >= 0; i--) {
      if (value >= thresholds[i]) {
        return i + 1;
      }
    }
    return 0;
  }
}
