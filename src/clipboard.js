
import { getLocalTimeStamp } from './helper.js';

export const Clipboard = {

  // data() {
  //   return {
  //     csvsep: ';',
  //     clipformat: 1, // FIXME
  //   };
  // },

  methods: {
    createClipboardContent() {
      switch (this.clipformat) {
        case 0: return this.createTextContent();
        case 1: return this.createCSVContent();
        case 2:	return this.createMarkdownContent();
        case 3: return this.createJSONContent();
        default:
          return 'Illegal format selected: ' + this.clipformat;
      }
    },

    contentRow(label, sep, arr) {
      let text = label;
      let digits = label === 'Std' ? Array(1, 1, 1) : this.digits;
      arr.forEach((val, i) => {
        text += sep + val.toFixed(digits[i]);
      });
      return text + '\n';
    },

    createTextContent() {
      let sep = ' ';
      let text = getLocalTimeStamp(new Date());
      text +=	'\nSystole/Diastole@Pulse\n';
      text += this.contentRow('Avg', sep, this.avg);
      text += this.contentRow('Min', sep, this.min);
      text += this.contentRow('Max', sep, this.max);
      text += this.contentRow('Std', sep, this.std);
      return text;
    },

    createCSVContent() {
      let sep = this.csvsep;
      let text = getLocalTimeStamp(new Date());
      text +=	`\n${sep}Systole${sep}Diastole${sep}Pulse\n`;
      text += this.contentRow('Avg', sep, this.avg);
      text += this.contentRow('Min', sep, this.min);
      text += this.contentRow('Max', sep, this.max);
      text += this.contentRow('Std', sep, this.std);
      return text;
    },

    createMarkdownContent() {
      let sep = '|';
      let text = getLocalTimeStamp(new Date());
      text +=	`\n${sep}${sep}Systole${sep}Diastole${sep}Pulse\n`;
      text += `${sep}-${sep}-${sep}-${sep}-\n`;
      text += this.contentRow('Avg', sep, this.avg);
      text += this.contentRow('Min', sep, this.min);
      text += this.contentRow('Max', sep, this.max);
      text += this.contentRow('Std', sep, this.std);
      return text;
    },

    createJSONContent() {
      function createObject(arr, digits) {
        return {
          Sys: Number(arr[0].toFixed(digits[0])),
          Dia: Number(arr[1].toFixed(digits[1])),
          Pulse: Number(arr[2].toFixed(digits[2])),
        };
      }

      return JSON.stringify({
        Date: getLocalTimeStamp(new Date()),
        Avg: createObject(this.avg, this.digits),
        Min: createObject(this.min, this.digits),
        Max: createObject(this.max, this.digits),
        Std: createObject(this.std, Array(1, 1, 1)),
      });
    },
  },
};
