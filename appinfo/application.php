<?php
namespace OCA\BPLog\AppInfo;

use OCA\BPLog\BPLogConfig;
use OCA\BPLog\Controller\SettingsController;
use OCP\AppFramework\App;

class Application extends App {
	public function __construct(array $urlParams=[]) {
		parent::__construct('bplog', $urlParams);
	}
}
