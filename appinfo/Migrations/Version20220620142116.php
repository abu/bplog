<?php
namespace OCA\bplog\Migrations;

use OCP\IDBConnection;
use OCP\Migration\ISqlMigration;

/**
 * Migrate configkeys in preferences
 */
class Version20220620142116 implements ISqlMigration {
	private const PREFS = 'preferences';
	private const APPID = 'bplog';

	private function migrateConfigKey($qb, $oldname, $newname) {
		$result = $qb->select('userid', 'configvalue')
			->from(self::PREFS)
			->where('appid = ?')
			->andWhere('configkey = ?')
			->setParameter(0, self::APPID)
			->setParameter(1, $oldname)
			->execute();

		while ($row = $result->fetch()) {
			$qb->insert(self::PREFS)
				->values(
					[
					'userid' => '?',
					'appid' => '?',
					'configkey' => '?',
					'configvalue' => '?',
				]
				)
			->setParameter(0, $row['userid'])
			->setParameter(1, self::APPID)
			->setParameter(2, $newname)
			->setParameter(3, $row['configvalue'])
			->execute();
		}
		$result->closeCursor();
	}

	private function deleteConfigKey($qb, $configkey) {
		$qb->delete(self::PREFS)
			->where('appid = ?')
			->andWhere('configkey = ?')
			->setParameter(0, self::APPID)
			->setParameter(1, $configkey)
			->execute();
	}

	public function sql(IDBConnection $dbc) {
		$this->migrateConfigKey($dbc->getQueryBuilder(), 'stats', 'showstats');
		$this->deleteConfigKey($dbc->getQueryBuilder(), 'stats');

		$this->migrateConfigKey($dbc->getQueryBuilder(), 'recent', 'histviewlen');
		$this->deleteConfigKey($dbc->getQueryBuilder(), 'recent');

		$this->migrateConfigKey($dbc->getQueryBuilder(), 'mode', 'histviewmode');
		$this->deleteConfigKey($dbc->getQueryBuilder(), 'mode');
	}
}
