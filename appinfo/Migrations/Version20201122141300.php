<?php
namespace OCA\bplog\Migrations;

use Doctrine\DBAL\Schema\Schema;
use OCP\Migration\ISchemaMigration;

 /**
  * Create initial tables for the app
  */
class Version20201122141300 implements ISchemaMigration {
	private $prefix;

	public function changeSchema(Schema $schema, array $options) {
		$this->prefix = $options['tablePrefix'];

		if (!$schema->hasTable("{$this->prefix}bplog_logs")) {
			$table = $schema->createTable("{$this->prefix}bplog_logs");

			$table->addColumn('id', 'integer', [
				'unsigned' => true,
				'autoincrement' => true,
				'notnull' => true,
				'length' => 11,
			]);
			$table->addColumn('systole', 'integer', [
				'length' => 4,
				'default' => 0,
			]);
			$table->addColumn('diastole', 'integer', [
				'length' => 4,
				'default' => 0,
			]);
			$table->addColumn('pulse', 'integer', [
				'length' => 4,
				'default' => 0,
			]);
			$table->addColumn('created', 'integer', [
				'length' => 8,
				'notnull' => false,
			]);
			$table->addColumn('user_id', 'string', [
				'length' => 64,
				'notnull' => true,
			]);
			$table->setPrimaryKey(['id']);
		}
	}
}
