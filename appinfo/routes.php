<?php

return [
	'resources' => [
		'log' 		=> ['url' => '/check'],
		'logapi' 	=> ['url' => '/api/v0.1/check'],
	],
  'routes' => [
		['name' => 'page#index', 	'url' => '/', 'verb' => 'GET'],

		['name' => 'log#get',		  'url' => '/check/{id}', 'verb' => 'GET'],
		['name' => 'log#export', 	'url' => '/export', 'verb' => 'GET'],
		['name' => 'log#import', 	'url' => '/import', 'verb' => 'POST'],

	// Settings

		['name' => 'settings#get', 'url' => '/config', 'verb' => 'GET'],
		['name' => 'settings#set', 'url' => '/config', 'verb' => 'POST'],

	// API

		['name' => 'logapi#export',	'url' => '/api/v0.1/export', 'verb' => 'GET'],
		['name' => 'logapi#get',		'url' => '/api/v0.1/check/{id}', 'verb' => 'GET'],
		['name' => 'logapi#preflighted_cors', 'url' => '/api/v0.1/{path}',
			 'verb' => 'OPTIONS', 'requirements' => ['path' => '.+']],
  ]
];
