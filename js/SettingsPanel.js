'use strict';

$(document).ready(function () {
  const CONFIG = OC.generateUrl('/apps/bplog/config');

  function getAge(birthday) {
    return new Date().getFullYear() - birthday.getFullYear();
  }

  function setAge(birthday) {
    const bd = new Date(birthday);
    if (bd instanceof Date && !isNaN(bd)) {
      $('#bp-age').val(getAge(bd));
    }
  }

  $('#bp-birthday').focusout(function (e) {
    setAge($(e.target).val());
  });

  async function setPrefs(key, value) {
    let url = CONFIG + '?key=' + key + '&value=';

    url += typeof value === 'boolean' ? (value ? '1' : '0') : value;

    let response = await fetch(url, {
      method: 'POST',
      headers: {
        'requesttoken': oc_requesttoken,
      },
    });
    if (response.ok) {
      await response.text();
    } else {
      OC.Notification.showTemporary(t('bplog', 'Request failed'));
    }
  }

  function getSelection(id) {
    return Number($(id).prop('selectedIndex'));
  }

  let currPrefs = {
    gender: getSelection('bp-gender'),
    birthday: $('#bp-birthday').val(),
    height: Number($('#bp-height').val()),
    csvsep: getSelection('#bp-csvsep'),
    clipformat: getSelection('#bp-clipformat'),
  };

  setAge(currPrefs.birthday);

  $('#bp-save').click(function (e) {
    e.preventDefault();

    try {
      OC.msg.startSaving('#bp-save-message');

      let gender = getSelection('#bp-gender');
      if (gender != currPrefs.gender) {
        setPrefs('gender', gender);
        currPrefs.gender = gender;
      }

      let birthday = $('#bp-birthday').val();
      if (birthday != currPrefs.birthday) {
        const bd = new Date(birthday);

        if (!(bd instanceof Date && !isNaN(bd))) {
          throw 'Date is invalid';
        }
        if ((new Date() - bd) <= 0) {
          throw 'Date must be in the past';
        }
        setPrefs('birthday', birthday);
        currPrefs.birthday = birthday;

        $('#bp-age').val(getAge(bd));
      }

      let csvsep = getSelection('#bp-csvsep');
      if (csvsep != currPrefs.csvsep) {
        setPrefs('csvsep', csvsep);
        currPrefs.csvsep = csvsep;
      }

      let clipformat = getSelection('#bp-clipformat');
      if (clipformat != currPrefs.clipformat) {
        setPrefs('clipformat', clipformat);
        currPrefs.clipformat = clipformat;
      }

      OC.msg.finishedSuccess('#bp-save-message', t('bplog', 'Preferences are saved'));
    } catch (msg) {
      OC.msg.finishedError('#bp-save-message', t('bplog', msg));
    }
  });
});
