# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

-	Nothing so far

## [v0.8.1](https://codeberg.org/abu/bplog/releases/tag/v0.8.1) - 2023-10-01

### Fixed

- Sending request token is now mandatory.

### Changed

- Updated fpdf to 1.86. Moved to fpdf vendor-libs.

## [v0.8.0](https://codeberg.org/abu/bplog/releases/tag/v0.8.0) - 2022-08-04

### Fixed

- Fix broken API for new PDF export

### Added

- Statistics min and max values show their timestamps as tooltip.
- Button to copy statistics to clipboard, supporting various formats.
- Option 'CSV-separator' added to Settings->Additional.
- Option 'Clipboard format' added to Settings->Additional.

## [v0.7.1](https://codeberg.org/abu/bplog/releases/tag/v0.7.1) - 2022-06-21

### Fixed

- Fixed broken DB-Migration step


## [v0.7.0](https://codeberg.org/abu/bplog/releases/tag/v0.7.0) - 2022-06-19

### Added

- Settings-Panel with personal details at Settings->Additional.
- Minor redesign of PDF export.

### Fixed

- App is not compliant [#1](https://codeberg.org/abu/bplog/issues/1)

## [v0.6.0](https://codeberg.org/abu/bplog/releases/tag/v0.6.0) - 2022-01-25

### Changed

- Omit seconds in pdf output
- Use QueryBuilder for DB-access
- Move statistics to frontend
- Use the core's timezone settings

### Added

- Display units at input fields
- Display version info in the settings menue
- Display standard deviation in statistics

## [v0.5.1](https://codeberg.org/abu/bplog/releases/tag/v0.5.1) - 2021-05-10

### Changed

- Removed all jQuery usage
- New extra module for date input

### Fixed

- Text alignment of export button

---

## [v0.5.0](https://codeberg.org/abu/bplog/releases/tag/v0.5.0) - 2021-03-05

### Added

- More export formats, now supports CSV, PDF and JSON.

### Changed

- History can be limited to a given number of recent checks, days or weeks.
- API: Error-handling improved.
- API: Default resource renamed to _/check_
- API: [Documentation](https://codeberg.org/abu/bplog/wiki/API-Doc) completed.

---

## [v0.4.0](https://codeberg.org/abu/bplog/releases/tag/v0.4.0) - 2020-12-06

### Added

- History can be limited to a given number of recent checks. Affects list and statistics.
- API completed

### Changed

- Using Vue.js
- Using DB Migrations
- Moving to codeberg.org

---

## [v0.3.1](https://codeberg.org/abu/bplog/releases/tag/v0.3.1) - 2019-08-18

Maintenance release, no new features.

---

## [v0.3.0](https://codeberg.org/abu/bplog/releases/tag/v0.3.0) - 2019-08-11

### Added

- Displays statistics with HT class mark.

---

## [v0.2.0](https://codeberg.org/abu/bplog/releases/tag/v0.2.0) - 2019-04-27

### Added

- Entering new records with specific timestamp.
- Editing of existing records.
- Import existing data from CSV-File.

---

## [v0.1.0](https://codeberg.org/abu/bplog/releases/tag/v0.1.0) - 2019-02-28

- Initial release

---
