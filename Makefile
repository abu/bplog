
.PHONY: all appstore build clean lint install

extract = $(shell grep $(1) $(CURDIR)/appinfo/info.xml | cut -d'>' -f2 | cut -d'<' -f1)

app_name = $(call extract,'id>')
app_version = $(call extract,'version>')
appinfo = src/appinfo.js

cert_dir=$(HOME)/.owncloud
private_key=$(cert_dir)/$(app_name).key
certificate=$(cert_dir)/$(app_name).crt

config_dir=../../config
occ=../../occ

build_dir=$(CURDIR)/build

appstore_build_dir=$(build_dir)/$(app_name)
package_file=$(build_dir)/$(app_name)

sign=php -f $(occ) integrity:sign-app --privateKey="$(private_key)" --certificate="$(certificate)"
sign_skip_msg="*** Signing skipped due to missing files."

ifneq (,$(wildcard $(private_key)))
ifneq (,$(wildcard $(certificate)))
ifneq (,$(wildcard $(occ)))
CAN_SIGN=true
endif
endif
endif

dev: lint
	npm run dev

prod: lint
	npm run prod

lint: $(appinfo)
	npm run lint

all: prod appstore

install:
	composer install
	npm install

clean:
	rm -rf $(build_dir)

###############################################################################
# Create javascript file containing current version info

.ONESHELL:
$(appinfo): appinfo/info.xml
	@cat <<- EOF > $@
	// Generated file, do not modify
	'use strict';
	const appinfo = {
		id: '$(app_name)',
		version: '$(app_version)',
	};
	export default appinfo;
	EOF

###############################################################################

appstore-dir:
	rm -rf $(appstore_build_dir)
	mkdir -p $(appstore_build_dir)

appstore: appstore-dir $(build_dir)/appstore-exclude
	rsync -r . $(appstore_build_dir) --exclude-from=$(build_dir)/appstore-exclude
ifdef CAN_SIGN
	mv $(config_dir)/config.php $(config_dir)/config-2.php
	$(sign) --path="$(appstore_build_dir)"
	mv $(config_dir)/config-2.php $(config_dir)/config.php
else
	@echo $(sign_skip_msg)
endif
	tar -C $(build_dir) -czf $(package_file).tar.gz  $(app_name)

###############################################################################
# Create exclude list for rsync

.ONESHELL:
$(build_dir)/appstore-exclude:
	@cat <<- EOF > $@
		.eslintignore
		.eslintrc.js
		.git
		.gitignore
		.php-cs-fixer.cache
		.php-cs-fixer.dist.php
		build/
		CHANGELOG.md
		composer.json
		composer.lock
		install
		Makefile
		node_modules/
		package-lock.json
		package.json
		screenshots/
		src/
		tools/
		vendor/
		webpack.config.js
	EOF

###############################################################################
# Run php-cs-fixer and check owncloud code-style

PHP_CS_FIXER=php -d zend.enable_gc=0 vendor/bin/php-cs-fixer fix -v --diff \
	--allow-risky yes

.PHONY: test-php-style
test-php-style:	vendor/bin/php-cs-fixer
	$(PHP_CS_FIXER) --dry-run

.PHONY: test-php-style-fix
test-php-style-fix:	vendor/bin/php-cs-fixer
	$(PHP_CS_FIXER)

###############################################################################
