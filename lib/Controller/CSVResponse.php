<?php

namespace OCA\BPLog\Controller;

use OCA\BPLog\BPLogConfig;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\Response;
use OC\HintException;

class CSVResponse extends Response {
	public const TIMEFORMAT = 'Y-m-d H:i:s';
	private $config;
	private $content;

	public function __construct(BPLogConfig $config, $content) {
		$user = \OC::$server->getUserSession()->getUser();
		if ($user === null) {
			throw new HintException('User not logged in');
		}

		$this->config = $config;
		$timezone = $this->config->getTimeZone();
		$sep = $this->config->getCSVSeparatorString();

		$csv = "Date${sep}Systole${sep}Diastole${sep}Pulse\n";

		foreach ($content as $log) {
			$date = $log->getTimestamp();
			$date->setTimezone($timezone);
			$csv .= $date->format(self::TIMEFORMAT)	. $sep;

			$csv .= $log->getSystole()		. $sep;
			$csv .= $log->getDiastole()	  . $sep;
			$csv .= $log->getPulse()			. "\n";
		}

		$export_name = '"BPLog (' . $user->getDisplayName() . ') (' . \date('Y-m-d') . ').csv"';

		$this->addHeader("Cache-Control", "private");
		$this->addHeader("Content-Type", "text/csv");
		$this->addHeader("Content-Length", \strlen($csv));
		$this->addHeader("Content-Disposition", "attachment; filename=" . $export_name);

		$this->content = $csv;
	}

	public function render() {
		return $this->content;
	}
}
