<?php

namespace OCA\BPLog\Controller;

use OCA\BPLog\BPLogConfig;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\DataResponse;
use OCP\IConfig;
use OCP\IRequest;

class SettingsController extends Controller {
	private $config;

	public function __construct($appName, IRequest $request, BPLogConfig $config) {
		parent::__construct($appName, $request);
		$this->config = $config;
	}

	/**
	 * Get configs for Settings-AppMenu and Clipboard.
	 * @NoAdminRequired
	 *
	 * @return DataResponse
	 */
	public function get() {
		return new DataResponse(array_merge(
			$this->config->getSettingsForMenu(),
			$this->config->getSettingsForClipboard()
		));
	}

	/**
	 * @NoAdminRequired
	 *
	 * @param string $key
	 * @param string $value
	 */
	public function set($key, $value) {
		$this->config->set($key, $value);
	}
}
