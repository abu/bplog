<?php

namespace OCA\BPLog\Controller;

use OCA\BPLog\BPLogConfig;
use OCA\BPLog\Service\LogService;
use OCP\AppFramework\ApiController;
use OCP\AppFramework\Http\DataResponse;
use OCP\IRequest;
use OCP\IUserSession;

class LogapiController extends ApiController {
	private $appname;
	private $config;
	private $service;
	private $userSession;

	use Errors;

	public function __construct(
		$appName,
		IRequest $request,
		IUserSession $userSession,
		LogService $service,
		BPLogConfig $config
	) {
		parent::__construct($appName, $request);

		$this->appname = $appName;
		$this->service = $service;
		$this->userSession = $userSession;
		$this->config = $config;
	}

	private function getUserId() {
		return $this->userSession->getUser()->getUID();
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @CORS
	 * @param int $histviewlen
	 */
	public function index($histviewlen = 0) {
		return $this->service->findAll($this->getUserId(), $histviewlen);
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @CORS
	 * @param int $id
	 */
	public function get($id) {
		return $this->respond(function () use ($id) {
			return $this->service->find($id, $this->getUserId());
		});
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @CORS
	 * @param int $sys
	 * @param int $dia
	 * @param int $hrt
	 */
	public function create($sys, $dia, $hrt) {
		return $this->respond(function () use ($sys, $dia, $hrt) {
			return $this->service->create(\time(), $sys, $dia, $hrt, $this->getUserId());
		});
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @CORS
	 * @param int $id
	 * @param int $created,
	 * @param int $systole,
	 * @param int $diastole,
	 * @param int $pulse
	 *
	 * @return DataResponse
	 */
	public function update($id, $created, $sys, $dia, $hrt) {
		return $this->respond(function () use ($id, $created, $sys, $dia, $hrt) {
			$this->service->update($id, $created, $sys, $dia, $hrt, $this->getUserId());
			return ['success' => true];
		});
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @CORS
	 * @param int $id
	 * @return DataResponse
	 */
	public function destroy($id) {
		return $this->respond(function () use ($id) {
			$this->service->destroy($id, $this->getUserId());
			return ['success' => true];
		});
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @CORS
	 */
	public function export() {
		$this->registerResponder('csv', function ($value) {
			if ($value instanceof DataResponse) {
				return new CSVResponse(
					$value->getData(),
					$value->getStatus(),
					$value->getHeaders()
				);
			} else {
				return new CSVResponse($this->config, $value);
			}
		});

		$this->registerResponder('pdf', function ($value) {
			if ($value instanceof DataResponse) {
				return new PDFResponse(
					$value->getData(),
					$value->getStatus(),
					$value->getHeaders()
				);
			} else {
				return new PDFResponse($this->config, $value);
			}
		});

		return $this->service->findAll($this->getUserId());
	}
}
