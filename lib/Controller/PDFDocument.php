<?php

namespace OCA\BPLog\Controller;

require('bplog/vendor-libs/autoload.php');

class PDFDocument extends \FPDF {
	protected const FONT = 'Arial';
	protected const TIMEFORMAT = 'Y-m-d H:i';
	protected const TITLE = 'Blood Pressure Log';

	protected $currCol = 0;
	protected $y0;
	protected $width = [ 45, 10, 10, 10 ];
	protected $height = 5;

	protected $timezone;
	protected $date;

	protected $name;
	protected $gender;
	protected $birthday;

	public function Configure($config) {
		$this->date = \date('Y-m-d');
		$this->timezone = new \DateTimeZone(
			\OC::$server->getConfig()->getUserValue($config['user']->getUID(), 'core', 'timezone')
		);

		$this->name = $config['user']->getDisplayName();
		$this->gender = $config['gender'];
		$this->birthday = $config['birthday'];

		$title = 'Blood Pressure Log (' . $this->name . ') (' . $this->date . ')';
		$this->SetTitle($title, true);	// doc title (utf8)
		$this->SetLeftMargin(25);
		$this->AddPage();
		$this->AliasNbPages();
		$this->SetFont(self::FONT, '', 12);
	}

	public function Item($log) {
		$w = &$this->width;
		$h = &$this->height;

		$date = $log->getTimestamp();
		$date->setTimezone($this->timezone);
		$this->Cell($w[0], $h, $date->format(self::TIMEFORMAT), 0, 0);
		$this->Cell($w[1], $h, $log->getSystole(), 0, 0, 'R');
		$this->Cell($w[2], $h, $log->getDiastole(), 0, 0, 'R');
		$this->Cell($w[3], $h, $log->getPulse(), 0, 1, 'R');
	}

	public function Header() {
		$this->SetFont(self::FONT, '', 12);
		$this->Cell(50, 10, self::TITLE);
		$this->Cell(110, 10, $this->date, 0, 1, 'R');

		$this->SetFont(self::FONT, 'B', 14);
		$this->Cell(50, 6, \utf8_decode($this->name), 0, 1);
		$this->SetFont(self::FONT, '', 10);
		$this->Cell(50, 6, $this->birthday .  ' ' . $this->gender);

		$this->SetFont(self::FONT, 'B', 12);
		$this->Ln(10);
		$this->y0 = $this->GetY();
		$this->ColumnHeader();
	}

	public function Footer() {
		$this->SetY(-15);
		$this->SetFont(self::FONT, '', 12);
		$this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
	}

	public function SetCol($col) {
		$this->currCol = $col;
		// $x = 10 + $col * 85;
		$x = 25 + $col * 85;
		$this->SetLeftMargin($x);
		$this->SetX($x);
	}

	public function AcceptPageBreak() {
		if ($this->currCol < 1) {
			$this->SetCol($this->currCol + 1);
			$this->SetY($this->y0);
			$this->ColumnHeader();
			return false;
		} else {
			$this->SetCol(0);
			return true;
		}
	}

	public function ColumnHeader() {
		$this->SetFont(self::FONT, 'B', 12);
		$w = &$this->width;
		$h = &$this->height;
		$this->Cell($w[0], $h, 'Timestamp', 'B', 0);
		$this->Cell($w[1], $h, 'Sys', 'B', 0, 'R');
		$this->Cell($w[1], $h, 'Dia', 'B', 0, 'R');
		$this->Cell($w[1], $h, 'Hrt', 'B', 1, 'R');
		$this->SetFont(self::FONT, '', 12);
	}
};
