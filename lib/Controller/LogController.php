<?php

namespace OCA\BPLog\Controller;

use OCA\BPLog\BPLogConfig;
use OCA\BPLog\Service\LogService;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\DataResponse;
use OCP\IRequest;

class LogController extends Controller {
	private $service;
	private $userId;
	private $config;
	protected $request;

	public function __construct(
		$AppName,
		IRequest $request,
		LogService $service,
		BPLogConfig $config,
		$userId
	) {
		parent::__construct($AppName, $request);

		$this->service = $service;
		$this->userId = $userId;
		$this->request = $request;
		$this->config = $config;
	}

	/**
	 * @NoAdminRequired
	 *
	 * @param int $histviewlen
	 * @param int $histviewmode
	 */
	public function index($histviewlen = 0, $histviewmode = 0) {
		return $this->service->findAll($this->userId, $histviewlen, $histviewmode);
	}

	/**
	 * @NoAdminRequired
	 *
	 * @param int $id
	 */
	public function get($id) {
		return $this->service->find($id, $this->userId);
	}

	/**
	 * @NoAdminRequired
	 *
	 * @param int $created,
	 * @param int $systole,
	 * @param int $diastole,
	 * @param int $pulse
	 *
	 * @return DataResponse
	 */
	public function create($created, $systole, $diastole, $pulse) {
		$this->service->create($created, $systole, $diastole, $pulse, $this->userId);
		return new DataResponse(['success' => true]);
	}

	/**
	 * @NoAdminRequired
	 *
	 * @param int $id
	 * @param int $created,
	 * @param int $systole,
	 * @param int $diastole,
	 * @param int $pulse
	 *
	 * @return DataResponse
	 */
	public function update($id, $created, $systole, $diastole, $pulse) {
		$this->service->update($id, $created, $systole, $diastole, $pulse, $this->userId);
		return new DataResponse(['success' => true]);
	}

	/**
	 * @NoAdminRequired
	 *
	 * @param int $id
	 *
	 * @return DataResponse
	 */
	public function destroy($id) {
		$this->service->destroy($id, $this->userId);
		return new DataResponse(['success' => true]);
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 */
	public function export() {
		$this->registerResponder('csv', function ($value) {
			if ($value instanceof DataResponse) {
				return new CSVResponse(
					$value->getData(),
					$value->getStatus(),
					$value->getHeaders()
				);
			} else {
				return new CSVResponse($this->config, $value);
			}
		});

		$this->registerResponder('pdf', function ($value) {
			if ($value instanceof DataResponse) {
				return new PDFResponse(
					$value->getData(),
					$value->getStatus(),
					$value->getHeaders()
				);
			} else {
				return new PDFResponse($this->config, $value);
			}
		});

		return  $this->service->findAll($this->userId);
	}

	/**
	 * @NoAdminRequired
	 *
	 * @param int $clear
	 *
	 * @return DataResponse
	 */
	public function import($clear) {
		$file = $this->request->getUploadedFile('bp-import');
		$error = [];

		if (empty($file)) {
			$error[] = 'No file provided for import';
		} else {
			if ($file['type'] === 'text/csv') {
				if ($clear === 1) {
					$this->service->deleteAll($this->userId);
				} //
				$error = $this->service->import($file['tmp_name'], $this->config); // FIXME
				if (empty($error)) {
					return new DataResponse(['success' => true]);
				}
			} else {
				$error[] = 'Unsupported file type for import';
			}
		}

		return new DataResponse(['success' => false, 'error' => $error[0]]);
	}
}
