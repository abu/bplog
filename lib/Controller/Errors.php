<?php

namespace OCA\BPLog\Controller;

use Closure;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\DataResponse;
use OCA\BPLog\Service\NotFoundException;

trait Errors {
	protected function respond(Closure $callback) {
		try {
			return new DataResponse($callback());
		} catch (NotFoundException $e) {
			$message = [
			  'success' => false,
			  'message' => 'Item not found'
			];
			return new DataResponse($message, Http::STATUS_NOT_FOUND);
		}
	}
}
