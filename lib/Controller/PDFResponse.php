<?php

namespace OCA\BPLog\Controller;

use OC\HintException;
use OCA\BPLog\BPLogConfig;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\Response;

class PDFResponse extends Response {
	private $content;

	public function __construct(BPLogConfig $appconfig, $data) {
		$user = \OC::$server->getUserSession()->getUser();
		if ($user === null) {
			throw new HintException('User not logged in');
		}

		$config = [
			'user' => $user,
			'gender' =>  $appconfig->getGender() ? 'F' : 'M',
			'birthday' => $appconfig->getBirthday(),
		];

		$export_name = '"BPLog (' . $user->getDisplayName() . ') (' . \date('Y-m-d') . ').pdf"';

		$this->createContent($config, $data);

		$this->addHeader("Cache-Control", "private");
		$this->addHeader("Content-Type", "application/pdf");
		$this->addHeader("Content-Length", \strlen($this->content));
		$this->addHeader("Content-Disposition", "attachment; filename=" . $export_name);
	}

	public function render() {
		return $this->content;
	}

	public function createContent($config, $data) {
		$pdfdoc = new PDFDocument();

		$pdfdoc->Configure($config);

		foreach ($data as $check) {
			$pdfdoc->Item($check);
		}

		$this->content = $pdfdoc->Output('S');
	}
}
