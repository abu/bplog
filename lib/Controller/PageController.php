<?php

namespace OCA\BPLog\Controller;

use \OCP\AppFramework\Controller;
use \OCP\AppFramework\Http\TemplateResponse;
use \OCP\IRequest;

class PageController extends Controller {
	public function __construct(
		$AppName,
		IRequest $request
	) {
		parent::__construct($AppName, $request);
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 */
	public function index() {
		return new TemplateResponse('bplog', 'main');
	}
}
