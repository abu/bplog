<?php

namespace OCA\BPLog\Service;

use OCA\BPLog\BPLogConfig;
use OCA\BPLog\Db\Log;
use OCA\BPLog\Db\LogMapper;
use OCP\AppFramework\Db\DoesNotExistException;
use OCP\AppFramework\Db\MultipleObjectsReturnedException;

use Exception;

class LogService {
	public const TIMEFORMAT = 'Y-m-d H:i:s';
	private $mapper;

	public function __construct(LogMapper $mapper) {
		$this->mapper = $mapper;
	}

	private function handleException($e) {
		if (
			$e instanceof DoesNotExistException ||
			$e instanceof MultipleObjectsReturnedException
		) {
			throw new NotFoundException($e->getMessage());
		} else {
			throw $e;
		}
	}

	public function findAll($userId, $histviewlen = 0, $histviewmode = 0) {
		return $this->mapper->findAll($userId, $histviewlen, $histviewmode);
	}

	public function find($id, $userId) {
		try {
			return $this->mapper->find($id, $userId);
		} catch (Exception $e) {
			$this->handleException($e);
		}
	}

	public function create($created, $systole, $diastole, $pulse, $userId) {
		try {
			$log = new Log();

			$log->setCreated($created);
			$log->setSystole($systole);
			$log->setDiastole($diastole);
			$log->setPulse($pulse);
			$log->setUserId($userId);

			return $this->mapper->insert($log);
		} catch (Exception $e) {
			$this->handleException($e);
		}
	}

	public function update($id, $created, $systole, $diastole, $pulse, $userId) {
		try {
			$log = $this->find($id, $userId);

			$log->setCreated($created);	// $created in ticks
			$log->setSystole($systole);
			$log->setDiastole($diastole);
			$log->setPulse($pulse);

			return $this->mapper->update($log);
		} catch (Exception $e) {
			$this->handleException($e);
		}
	}

	public function destroy($id, $userId) {
		try {
			$log = $this->mapper->find($id, $userId);
			$this->mapper->delete($log);
		} catch (Exception $e) {
			$this->handleException($e);
		}
	}

	public function import($file, BPLogConfig $config) {
		$timezone = $config->getTimeZone();
		$sep = $config->getCSVSeparatorString();
		$rc = [];

		if (($handle = \fopen($file, 'r')) !== false) {
			\fgets($handle, 64);
			$row = 1;

			while (($data = \fgetcsv($handle, 64, $sep)) !== false) {
				if (\count($data) !== 4) {
					$rc[] = 'Syntax error at line ' . $row;
					break;
				}

				$date = \DateTime::createFromFormat(self::TIMEFORMAT, $data[0], $timezone);
				if ($date === false) {
					$rc[] = 'Invalid date at line ' . $row;
					break;
				}

				$this->create($date->getTimestamp(), $data[1], $data[2], $data[3], $config->getUserId());
				$row++;
			}

			\fclose($handle);
		} else {
			$rc[] = 'fopen error';
		}

		return $rc;
	}

	public function deleteAll($userId) {
		$this->mapper->deleteAll($userId);
	}
}
