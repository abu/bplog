<?php

namespace OCA\BPLog\Db;

use OCP\AppFramework\Db\Mapper;
use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\IDBConnection;

class LogMapper extends Mapper {
	public const TABLE = 'bplog_logs';
	public const ENTITY = 'OCA\BPLog\Db\Log';

	private $dbc;

	public function __construct(IDBConnection $dbc) {
		parent::__construct($dbc, self::TABLE, self::ENTITY);
		$this->dbc = $dbc;
	}

	public function find($id, $userId) {
		$qb = $this->dbc->getQueryBuilder();
		$qb->select('*')->from(self::TABLE)
		->where("id = $id")
		->andWhere("user_id = '$userId'");
		return $this->findEntity($qb->getSQL());
	}

	public function findAll($userId, $histviewlen, $histviewmode) {
		return $this->findEntities($this->buildQuery($histviewlen, $histviewmode), [$userId]);
	}

	public function deleteAll($userId) {
		$qb = $this->dbc->getQueryBuilder();
		$qb->delete(self::TABLE)
		->where("user_id = '$userId'")
		->execute();
	}

	private function buildQuery($histviewlen, $histviewmode) {
		$qb = $this->dbc->getQueryBuilder();
		$qb->select('*')->from(self::TABLE)->where('user_id = ?');

		if ($histviewlen) {
			switch ($histviewmode) {
		  default:
		  case 0: // checks
			$qb->orderBy('created', 'DESC')->setMaxResults($histviewlen);
			break;

		  case 1: // days
			$since = \time() - $histviewlen * 24 * 3600;
			$qb->andWhere("created > $since")->orderBy('created', 'DESC');
			break;

		  case 2: // weeks
			$since = \time() - $histviewlen * 7 * 24 * 3600;
			$qb->andWhere("created > $since")->orderBy('created', 'DESC');
			break;
			}
		} else {
			$qb->orderBy('created', 'DESC');
		}

		return $qb->getSQL();
	}
}
