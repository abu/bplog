<?php

namespace OCA\BPLog;

use OCP\IConfig;
use OCP\IUserSession;

class BPLogConfig {
	private const APPNAME = 'bplog';

	// Settings for SettingsPanel

	private const GENDER = 'gender';
	private const	BIRTHDAY = 'birthday';
	private const HEIGHT = 'height';
	private const CSVSEP = 'csvsep';
	private const CLIPFORMAT = 'clipformat';

	// Settings for AppMenu

	private const NEWONTOP = 'newontop';
	private const STATS = 'showstats';
	private const HISTVIEWLEN = 'histviewlen';
	private const HISTVIEWMODE = 'histviewmode';
	private const FORMAT = 'format';

	private $config;
	private $userSession;

	/**
	 * @param IConfig $config
	 * @param IUserSession $userSession
	 */
	public function __construct(IConfig $config, IUserSession $userSession) {
		$this->config = $config;
		$this->userSession = $userSession;
	}

	/////////////////////////////////////////////////////////////////////////////
	// Getter
	/////////////////////////////////////////////////////////////////////////////

	/**
	 * @return  string
	 */
	public function getUserId() {
		return $this->userSession->getUser()->getUID();
	}

	/**
	 * Get the users timezone from core
	 * @return  DateTimeZone
	 */
	public function getTimeZone() {
		return new \DateTimeZone($this->config->getUserValue($this->getUserId(), 'core', 'timezone'));
	}

	/**
	 * @return  string
	 * 	';'
	 * 	','
	 */
	public function getCSVSeparatorString() {
		return $this->getUserValue(self::CSVSEP, '0') === '0' ? ';' : ',';
	}

	/**
	 * Get settings for SettingsPanel
	 * @return array
	 */
	public function getSettingsForPanel() {
		return [
			self::GENDER => $this->getGender(),
			self::BIRTHDAY => $this->getBirthday(),
			self::HEIGHT => $this->getHeight(),
			self::CSVSEP => $this->getCSVSeparator(),
			self::CLIPFORMAT => $this->getClipboardFormat(),
		];
	}

	/**
	 * Get settings for Clipboard
	 * @return array
	 */
	public function getSettingsForClipboard() {
		return [
			self::CSVSEP => $this->getCSVSeparator(),
			self::CLIPFORMAT => $this->getClipboardFormat(),
		];
	}

	/**
	 * Get settings for AppMenu
	 * @return array
	 */
	public function getSettingsForMenu() {
		return [
			self::NEWONTOP => $this->getNewOnTop(),
			self::STATS=> $this->getShowStats(),
			self::HISTVIEWLEN => $this->getHistoryViewLength(),
			self::HISTVIEWMODE => $this->getHistoryViewMode(),
			self::FORMAT => $this->getExportFormat(),
		];
	}

	private function getUserValue($key, $default) {
		return $this->config->getUserValue($this->getUserId(), self::APPNAME, $key, $default);
	}

	/**
	 * @return  int
	 * 	0 = Male
	 * 	1 = Female
	 */
	public function getGender() {
		return \intval($this->getUserValue(self::GENDER, '0'));
	}

	/**
	 * @return string Birthday in ISO format '1952-04-02'
	 */
	public function getBirthday() {
		return $this->getUserValue(self::BIRTHDAY, '1952-04-02');
	}

	/**
	 * @return float Height im meters
	 */
	public function getHeight() {
		return \floatval($this->getUserValue(self::HEIGHT, '1.82'));
	}

	/**
	 * @return  int
	 * 	0 = ';'
	 * 	1 = ','
	 */
	public function getCSVSeparator() {
		return \intval($this->getUserValue(self::CSVSEP, '0'));
	}

	/**
	 * @return  int
	 * 	0 = Text
	 * 	1 = CSV
	 *  2 = Markdown
	 *  3 = JSON
	 */
	public function getClipboardFormat() {
		return \intval($this->getUserValue(self::CLIPFORMAT, '1'));
	}

	/**
	 * @return int
	 *	0 = Oldest on top
	 *	1 = Newest on top
	 */
	public function getNewOnTop() {
		return \intval($this->getUserValue(self::NEWONTOP, 1));
	}

	/**
	 * @return int
	 *	0 = Don't show stats
	 *	1 = Show stats
	 */
	public function getShowStats() {
		return \intval($this->getUserValue(self::STATS, 1));
	}

	/**
	 * @return int
	 *	History view length
	 */
	public function getHistoryViewLength() {
		return \intval($this->getUserValue(self::HISTVIEWLEN, 0));
	}

	/**
	 * @return int
	 *	 0 = Show number of Checks
	 *	 1 = Days
	 *	 2 = Weeks
	 */
	public function getHistoryViewMode() {
		return \intval($this->getUserValue(self::HISTVIEWMODE, 0));
	}

	/**
	 * @return int
	 *	0 = CSV
	 *	1 = PDF
	 *	2 = JSON
	 */
	public function getExportFormat() {
		return \intval($this->getUserValue(self::FORMAT, 0));
	}

	/////////////////////////////////////////////////////////////////////////////
	// Setter
	/////////////////////////////////////////////////////////////////////////////

	public function set($key, $value) {
		switch ($key) {

			// Settings for AppMenu

			case self::NEWONTOP: $this->setNewOnTop($value); break;
			case self::STATS: $this->setShowStats($value); break;
			case self::HISTVIEWLEN: $this->setHistoryViewLength($value); break;
			case self::HISTVIEWMODE: $this->setHistoryViewMode($value); break;
			case self::FORMAT: $this->setExportFormat($value); break;

			// Settings for SettingsPanel

			case self::GENDER: $this->setGender($value); break;
			case self::BIRTHDAY: $this->setBirthday($value); break;
			case self::HEIGHT: $this->setHeight($value); break;
			case self::CSVSEP: $this->setCSVSeparator($value); break;
			case self::CLIPFORMAT: $this->setClipboardFormat($value); break;

			default: break;
		}
	}

	private function setUserValue($key, $value) {
		$this->config->setUserValue($this->getUserId(), self::APPNAME, $key, (string)$value);
	}

	/**
	 * @param int
	 * 	0 = Male
	 * 	1 = Female
	 */
	public function setGender($gender) {
		$this->setUserValue(self::GENDER, $gender);
	}

	/**
	 * @param string $birthday in ISO format '1952-04-02'
	 */
	public function setBirthday($birthday) {
		$this->setUserValue(self::BIRTHDAY, $birthday);
	}

	/**
	 * @param float Height in meters
	 */
	public function setHeight($height) {
		$this->setUserValue(self::HEIGHT, $height);
	}

	/**
	 * @param int
	 * 	0 = ';'
	 * 	1 = ','
	 */
	public function setCSVSeparator($csvsep) {
		$this->setUserValue(self::CSVSEP, $csvsep);
	}

	/**
	 * @param int
	 * 	0 = Text
	 * 	1 = CSV
	 *  2 = Markdown
	 *  3 = JSON
	 */
	public function setClipboardFormat($clipformat) {
		$this->setUserValue(self::CLIPFORMAT, $clipformat);
	}

	/**
	* @param int
	*	0 = Oldest on top
	*	1 = Newest on top
	*/
	public function setNewOnTop($newontop) {
		$this->setUserValue(self::NEWONTOP, $newontop);
	}

	/**
	 * @param int
	 *	0 = Don't show stats
	 *	1 = Show stats
	 */
	public function setShowStats($stats) {
		$this->setUserValue(self::STATS, $stats);
	}

	/**
	 * @param int
	 *	History view length
	 */
	public function setHistoryViewLength($histviewlen) {
		$this->setUserValue(self::HISTVIEWLEN, $histviewlen);
	}

	/**
	 * @param int
	 *	 0 = Show number of Checks
	 *	 1 = Days
	 *	 2 = Weeks
	 */
	public function setHistoryViewMode($mode) {
		$this->setUserValue(self::HISTVIEWMODE, $mode);
	}

	/**
	 * @param int
	 *	0 = CSV
	 *	1 = PDF
	 *	2 = JSON
	 */
	public function setExportFormat($format) {
		$this->setUserValue(self::FORMAT, $format);
	}
}
