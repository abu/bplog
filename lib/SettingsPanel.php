<?php

namespace OCA\BPLog;

use OCA\BPLog\BPLogConfig;
use OCP\Settings\ISettings;
use OCP\Template;

class SettingsPanel implements ISettings {
	public const PRIORITY = 10;

	private $config;

	public function __construct(BPLogConfig $config) {
		$this->config = $config;
	}

	public function getSectionID() {
		return 'additional';
	}

	public function getPriority() {
		return self::PRIORITY;
	}

	public function getPanel() {
		$tmpl =  new Template('bplog', 'SettingsPanel');
		$params = $this->config->getSettingsForPanel();
		foreach ($params as $key => $value) {
			$tmpl->assign($key, $value);
		}
		return $tmpl;
	}
}
