<?php
style('bplog', 'bplog');
script('bplog', 'SettingsPanel');
?>

<form class='section'>
	<h2 class='app-name'>Blood Pressure Log</h2>
	<h3>Personal Details</h3>
	<div>
		<label class='input' for='gender'>Gender</label>
		<select class='select setting panel' id='bp-gender'>
			<option value='0' <?php p($_['gender'] === 0 ? 'selected' : '') ?> >Male</option>
			<option value='1' <?php p($_['gender'] === 1 ? 'selected' : '') ?> >Female</option>
		</select>
	</div>

	<div>
		<label for="bp-birthday">Birthday:</label>
		<input type="date" id="bp-birthday" name="bp-birthday" value="<?php p($_['birthday']) ?>">
	</div>

	<div>
		<label for='age'>Age:</label>
		<input type='text' id='bp-age' disabled />
	</div>

	<h3>Misc</h3>
	<div>
		<label class='input' for='csvsep'>CSV separator</label>
		<select class='select setting panel' id='bp-csvsep'>
			<option value='0' <?php p($_['csvsep'] === 0 ? 'selected' : '') ?> >;</option>
			<option value='1' <?php p($_['csvsep'] === 1 ? 'selected' : '') ?> >,</option>
		</select>
	</div>

	<div>
		<label class='input' for='clipformat'>Clipboard fmt</label>
		<select class='select setting panel' id='bp-clipformat'>
			<option value='0' <?php p($_['clipformat'] === 0 ? 'selected' : '') ?> >Text</option>
			<option value='1' <?php p($_['clipformat'] === 1 ? 'selected' : '') ?> >CSV</option>
			<option value='2' <?php p($_['clipformat'] === 2 ? 'selected' : '') ?> >Markdown</option>
			<option value='3' <?php p($_['clipformat'] === 3 ? 'selected' : '') ?> >JSON</option>
		</select>
	</div>

	<button id="bp-save">Save</button>
	<span id="bp-save-message" class="msg"></span>
</form>
