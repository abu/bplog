# Blood Pressure Log

**[ownCloud](https://owncloud.org) app for tracking your blood pressure**

![](https://codeberg.org/abu/bplog/raw/branch/master/screenshots/main.png)

Instead of writing down your blood pressure measurements into a small booklet, known as *blood pressure passport*, you can put them into this app. **Benefit:** You always can see the average values over all measurements.

Please note, that I've created this app primarily for my personal use. I know that it might not be feature-complete, but it fits my requirements as it is.   

## Features
* Displays your statistic values, average, minimum and maximum.
* Adding new records, optionally with specific timestamp.
* Editing of existing records
* Export/import your tracking history to/from a CSV-File.
* RESTful API
